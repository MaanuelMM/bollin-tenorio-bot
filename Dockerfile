FROM python:3.10.5-alpine

# app dependencies
RUN apk add --no-cache ffmpeg

# app workdir
WORKDIR /usr/src/app

# pip dependencies
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# app files
COPY ./app/. .

# non-root user creation (Heroku creates it automatically)
# RUN adduser -D app
# USER app

# app execution (scripted in heroku.yml)
# CMD [ "python3", "./bot.py" ]
