# bollin-tenorio-bot

A Telegram bot to challenge myself. Original project started on GitHub: https://github.com/MaanuelMM/bollin-tenorio-bot

## Build Status
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/MaanuelMM/bollin-tenorio-bot?style=for-the-badge)