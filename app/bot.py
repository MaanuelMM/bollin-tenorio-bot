#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Authors:      MaanuelMM
# Credits:      eternnoir, atlink, CoreDumped-ETSISI, Eldinnie
# Created:      2019/02/14
# Last update:  2021/01/01

import os
import re
import logger
import telebot
import more_itertools

from bot_requests import get_token, get_arrive_stop, get_bicimad, get_parkings, get_generic_request
from data_loader import DataLoader
from transcriber import transcribe
from flask import Flask, request
from flask_sslify import SSLify
from unidecode import unidecode
from bs4 import BeautifulSoup
from logger import get_logger
from io import BytesIO


API_RESPONSE_ERROR_MESSAGE = "Unable to get API response."

logger = get_logger("bot", True)
logger.info("Starting bot...")

try:
    logger.info("Getting data and config vars...")
    data = DataLoader()

    logger.info("Creating TeleBot...")
    if data.THREAD_COUNT > 1:
        bot = telebot.TeleBot(token=data.TOKEN, threaded=True, num_threads=data.THREAD_COUNT)
    else:
        bot = telebot.TeleBot(token=data.TOKEN, threaded=False)

    logger.info("Creating Flask server...")
    server = Flask(__name__)

    if 'DYNO' in os.environ:  # only trigger SSLify if the app is running on Heroku
        sslify = SSLify(server)

except Exception as e:
    logger.info("Error creating Bot. Shutting down...")
    logger.error(e, exc_info=True)
    exit()


def log_message(message):
    logger.info("Received: \"" + message.text + "\" from " + message.from_user.first_name +
                " (ID: " + str(message.from_user.id) + ") " + "[Chat ID: " + str(message.chat.id) + "].")


def log_audio(message, mime_type):
    logger.info("Received " + mime_type + " from " + message.from_user.first_name + " (ID: " +
                str(message.from_user.id) + ") " + "[Chat ID: " + str(message.chat.id) + "].")


def log_api_error(response):
    logger.info("Error in API request:\n" + response)


def get_text_with_no_command(text):
    try:
        return text.split(" ", 1)[1]
    except:
        return ""


def is_integer(string):
    try:
        return int(string)
    except:
        return False


def open_file_stream(file):
    return BytesIO(file)


def close_file_stream(file_stream):
    file_stream.close()


def make_arrival_line(arrival):
    return "\n\nLínea " + arrival["line"] + " (" + arrival["destination"] + "):\n    " + arrival["estimateArrive"]


def process_time_left(time_left):
    if(str(time_left).isdecimal()):
        time_left = int(time_left)
        if(time_left == 0):
            return "En parada"
        elif(time_left > 2700):
            return ">45:00min"
        else:
            return "  " + str(int(time_left / 60)).zfill(2) + ":" + str(int(time_left % 60)).zfill(2) + "min"
    else:
        return "  --:--min"


# https://stackoverflow.com/questions/4391697/find-the-index-of-a-dict-within-a-list-by-matching-the-dicts-value
def sort_arrivals(arrivals):
    arrivals_sorted = []
    for arrival in arrivals:
        index_list = list(more_itertools.locate(
            arrivals_sorted, pred=lambda d: d["line"] == arrival["line"]))
        arrival["estimateArrive"] = process_time_left(
            arrival["estimateArrive"])
        if(index_list):  # False if empty
            arrivals_sorted[index_list[0]
                            ]["estimateArrive"] += "    " + arrival["estimateArrive"]
        else:
            arrivals_sorted.append(arrival)
        del index_list
    return arrivals_sorted


def process_arrival_response(arrivals):
    result = ""
    arrivals = sort_arrivals(
        sorted(arrivals, key=lambda d: (d["estimateArrive"])))
    for arrival in arrivals:
        result += make_arrival_line(arrival)
    return result + "\n\n"


def map_link_maker(coord_x, coord_y):
    return "https://google.com/maps/search/?api=1&query=" + str(coord_x) + "," + str(coord_y)


def bicimad_light(light):
    switcher_light = {
        0: "🟩 BAJA 🟩",
        1: "🟨 MEDIA 🟨",
        2: "🟥 ALTA 🟥"
    }
    return switcher_light.get(light, "⚠ INDEFINIDO ⚠")


def process_bicimad_response(bicimad):
    line = "\n"
    line += "Estación BiciMAD: " + \
        bicimad["name"] + " (#" + str(bicimad["id"]) + ")"
    if bool(bicimad["activate"]):
        line += "  🟢 OPERATIVA 🟢"
        line += "\n"
        line += "\n  Huecos disponibles: " + str(bicimad["free_bases"])
        line += "\n  Bicicletas disponibles: " + str(bicimad["dock_bikes"])
        line += "\n  Nivel de ocupación: " + bicimad_light(bicimad["light"])
    else:
        line += "  🔴 INOPERATIVA 🔴"
    line += "\n"
    line += "\n📍 Ubicación: " + \
        map_link_maker(bicimad["geometry"]["coordinates"]
                       [1], bicimad["geometry"]["coordinates"][0])
    line += "\n"
    return line


def make_parking_line(parking):
    line = "\n"
    line += parking["name"] + " (#" + str(parking["id"]) + ")"
    line += "\n\tPlazas libres: "
    if parking["freeParking"] is None:
        line += "Indefinido"
    else:
        line += str(parking["freeParking"])
    line += "\n"
    return line


def process_parkings_response(parkings):
    result = "\n"
    for parking in parkings:
        result += make_parking_line(parking)
    return result


def generate_metro_data_response(metro_data):
    response = {}
    soup = BeautifulSoup(metro_data, "html.parser")
    for img in soup.find_all("img"):
        line = img.get('class')[0].replace("-", " ").upper()
        estimations = re.sub(r'\s\s+', r'\n    ',
                             img.parent.next_sibling.next_sibling.text).strip()
        if line not in response:
            response[line] = []
        response[line].append(estimations)
    return response


def process_metro_response(metro_data):
    result = ""
    for k, v in generate_metro_data_response(metro_data).items():
        result += "\n\n"
        result += k.replace("LINEA", "LÍNEA")
        for item in v:
            result += "\n  "
            result += item
    return result


def message_sender(message, reply):
    if(len(reply) > 3000):
        splitted_reply = reply.split("\n\n")
        new_reply = ""
        for fragment in splitted_reply:
            if not new_reply:
                new_reply = fragment
            elif(len(new_reply + "\n\n" + fragment) > 3000):
                bot.reply_to(message, new_reply)
                new_reply = fragment
            else:
                new_reply += "\n\n" + fragment
        del new_reply
    else:
        bot.reply_to(message, reply)


def get_token_clean():
    try:
        token_response = get_token(
            data.EMTMADRID_GETTOKENSESSIONURL, data.EMTMADRID_EMAIL, data.EMTMADRID_PASSWORD)
        token = token_response["data"][0]["accessToken"]
        del token_response
    except:
        try:
            log_api_error(token_response)
        except:
            log_api_error(API_RESPONSE_ERROR_MESSAGE)
        raise
    return token


def get_arrive_stop_clean(stop_id):
    try:
        arrive_stop_response = get_arrive_stop(
            data.EMTMADRID_GETARRIVESTOPURL, get_token_clean(), stop_id, data.EMTMADRID_GETARRIVESTOPJSON)
        try:
            arrive_stop = arrive_stop_response["data"][0]["Arrive"]
        except:
            arrive_stop = ""
        del arrive_stop_response
    except:
        try:
            log_api_error(arrive_stop_response)
        except:
            log_api_error(API_RESPONSE_ERROR_MESSAGE)
        raise
    return arrive_stop


def get_bicimad_clean(station_id):
    try:
        bicimad_response = get_bicimad(
            data.EMTMADRID_GETBICIMADSTATIONSURL, get_token_clean(), station_id)
        try:
            bicimad = bicimad_response["data"]
        except:
            bicimad = ""
        del bicimad_response
    except:
        try:
            log_api_error(bicimad_response)
        except:
            log_api_error(API_RESPONSE_ERROR_MESSAGE)
        raise
    return bicimad


def get_parkings_clean():
    try:
        parkings_response = get_parkings(
            data.EMTMADRID_GETPARKINGSSTATUSURL, get_token_clean())
        try:
            parkings = parkings_response["data"]
        except:
            parkings = ""
        del parkings_response
    except:
        try:
            log_api_error(parkings_response)
        except:
            log_api_error(API_RESPONSE_ERROR_MESSAGE)
        raise
    return parkings


def get_metro_arrival_clean(station_id):
    try:
        metro_response = get_generic_request(
            data.METROMADRID_GETARRIVESTATIONURL + station_id).json()
        try:
            metro = metro_response[0]["data"]
        except:
            metro = ""
        del metro_response
    except:
        log_api_error("Unable to get API response due to unauthorized access.")
        raise
    return metro


def get_metro_stations_clean():
    try:
        return get_generic_request(data.METROMADRID_GETSTATIONSLISTURL).json()
    except:
        log_api_error("Unable to get Metro Madrid stations from GitHub.")
        raise


@bot.message_handler(commands=['start'])
def send_start(message):
    log_message(message)
    bot.reply_to(message, data.START)


@bot.message_handler(commands=['help'])
def send_help(message):
    log_message(message)
    bot.reply_to(message, data.HELP)


@bot.message_handler(commands=['hola'])
def send_hola(message):
    log_message(message)
    bot.reply_to(message, data.HOLA + message.from_user.first_name)


@bot.message_handler(commands=['parada'])
def send_parada(message):
    log_message(message)
    text = get_text_with_no_command(message.text)
    if(str(message.chat.id) in data.PARADA_CHAT_LIST and text.upper() in data.PARADA_CHAT_LIST[str(message.chat.id)]):
        text = data.PARADA_CHAT_LIST[str(message.chat.id)][text.upper()]
    if is_integer(text):
        try:
            arrive_stop = get_arrive_stop_clean(text)
            if arrive_stop:
                message_sender(message, data.PARADA_SUCCESSFUL.replace(
                    "<stopId>", text) + process_arrival_response(arrive_stop) + data.PARADA_SUCCESSFUL_DISCLAIMER)
            else:
                bot.reply_to(message, data.PARADA_NO_ESTIMATION)
            del arrive_stop
        except Exception as e:
            logger.error(e, exc_info=True)
            bot.reply_to(message, data.REQUEST_FAIL)
    else:
        bot.reply_to(message, data.PARADA_BAD_SPECIFIED)
    del text


@bot.message_handler(commands=['bicimad'])
def send_bicimad(message):
    log_message(message)
    text = get_text_with_no_command(message.text)
    if is_integer(text):
        try:
            bicimad = get_bicimad_clean(text)
            if bicimad:
                message_sender(message, process_bicimad_response(bicimad[0]))
            else:
                bot.reply_to(message, data.BICIMAD_NO_INFO)
            del bicimad
        except Exception as e:
            logger.error(e, exc_info=True)
            bot.reply_to(message, data.REQUEST_FAIL)
    else:
        bot.reply_to(message, data.BICIMAD_BAD_SPECIFIED)
    del text


@bot.message_handler(commands=['parkings'])
def send_parkings(message):
    log_message(message)
    try:
        message_sender(message, data.PARKINGS +
                       process_parkings_response(get_parkings_clean()))
    except Exception as e:
        logger.error(e, exc_info=True)
        bot.reply_to(message, data.REQUEST_FAIL)


@bot.message_handler(commands=['metro'])
def send_metro(message):
    log_message(message)
    station = get_text_with_no_command(message.text)
    if station:
        station = re.sub(r'[^a-zA-Z0-9 ]', r'',
                         unidecode(station).replace("-", " ")).upper()
        try:
            stations = get_metro_stations_clean()
            if station in stations:
                response = get_metro_arrival_clean(stations[station])
                if response:
                    message_sender(message, data.METRO_SUCCESSFUL +
                                   process_metro_response(response))
                else:
                    bot.reply_to(message, data.METRO_NO_ESTIMATION)
                del response
            else:
                bot.reply_to(message, data.METRO_DOES_NOT_EXIST)
            del stations
        except Exception as e:
            logger.error(e, exc_info=True)
            bot.reply_to(message, data.REQUEST_FAIL)
    else:
        bot.reply_to(message, data.METRO_BAD_SPECIFIED)
    del station


def audio(message):
    return message.audio.file_id


def voice(message):
    return message.voice.file_id


def video(message):
    return message.video.file_id


def video_note(message):
    return message.video_note.file_id


# https://stackoverflow.com/a/47378377
def file_switch(content_type):
    return {
        'audio': audio,
        'voice': voice,
        'video': video,
        'video_note': video_note
    }.get(content_type)


@bot.message_handler(content_types=['audio', 'voice'])
def transcribe_message(message):
    log_audio(message, message.content_type)
    try:
        msg = bot.reply_to(message, '*Texto*:', parse_mode='Markdown')
        try:
            audio_info = bot.get_file(
                file_switch(message.content_type)(message))
        except Exception as e:
            logger.error(e, exc_info=True)
            bot.edit_message_text(text=data.TRANSCRIPTION_FILE_SIZE_EXCEDED_ERROR,
                                  chat_id=message.chat.id, message_id=msg.message_id, parse_mode='Markdown')
        else:
            audio_file = bot.download_file(audio_info.file_path)
            audio_stream = open_file_stream(audio_file)
            edited_text = None
            edited_fragment = None
            for fragment in transcribe(audio_stream, data.WITAI_TOKEN):
                edited_fragment = fragment.strip()
                if edited_fragment:
                    logger.info("Previous message text: " + msg.text)
                    logger.info("New fragment: " + edited_fragment)
                    edited_text = msg.text.replace('\n', '').replace(
                        'Texto:', '*Texto*:\n_').replace('Continuación:', '*Continuación*:\n_')
                    if len(edited_text+' '+edited_fragment+'_') > 3000:
                        msg = bot.reply_to(
                            message, '*Continuación*: \n_'+edited_fragment+'_', parse_mode='Markdown')
                    else:
                        msg = bot.edit_message_text(
                            text=edited_text+' '+edited_fragment+'_', chat_id=message.chat.id, message_id=msg.message_id, parse_mode='Markdown')
            close_file_stream(audio_stream)
    except Exception as e:
        logger.error(e, exc_info=True)
        bot.reply_to(message, data.REQUEST_FAIL)
    finally:
        try:
            del msg, audio_info, audio_file, audio_stream, edited_text, edited_fragment
        except:
            pass


@server.route("/" + data.TOKEN, methods=['POST'])
def get_message():
    bot.process_new_updates(
        [telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
    return "!", 200


@server.route("/")
def webhook():
    logger.info("Removing current webhook...")
    bot.remove_webhook()
    logger.info("Successfully removed current webhook.")
    logger.info("Creating new webhook...")
    bot.set_webhook(url=data.URL+data.TOKEN)
    logger.info("Successfully created new webhook.")
    return "!", 200


if __name__ == "__main__":
    try:
        logger.info("Starting running server...")
        server.run(host="0.0.0.0", port=int(data.PORT))
    except Exception as e:
        logger.info("Error starting server. Shutting down...")
        logger.error(e, exc_info=True)
        exit()
