#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Authors:      carloalbertobarbano, MaanuelMM
# Credits:      carloalbertobarbano
# Created:      2020/01/30
# Last update:  2020/06/22

import traceback
import requests
import logging
import pydub
import io

from time import sleep
from pydub import AudioSegment


logger = logging.getLogger("speech")


def __transcribe_chunk(chunk, api_key):
    logging.debug("Using key: %s", api_key)

    headers = {
        'authorization': 'Bearer ' + api_key,
        'accept': 'application/vnd.wit.20200513+json',
        'content-type': 'audio/raw;encoding=signed-integer;bits=16;rate=8000;endian=little',
    }

    text = None

    try:
        request = requests.request(
            "POST",
            "https://api.wit.ai/speech",
            headers=headers,
            params={'verbose': True},
            data=io.BufferedReader(io.BytesIO(chunk.raw_data))
        )

        res = request.json()

        if 'text' in res:
            text = res['text']

    except:
        logger.error("Could not transcribe chunk: %s", traceback.format_exc())

    return text


def __generate_chunks(segment, length=20000/1001, split_on_silence=False, noise_threshold=-36):
    chunks = list()

    if split_on_silence is False:
        for i in range(0, len(segment), int(length*1000)):
            chunks.append(segment[i:i+int(length*1000)])

    else:
        while len(chunks) < 1:
            logger.debug('split_on_silence (threshold %d)', noise_threshold)
            chunks = pydub.silence.split_on_silence(segment, noise_threshold)
            noise_threshold += 4

        for i, chunk in enumerate(chunks):
            if len(chunk) > int(length*1000):
                subchunks = __generate_chunks(
                    chunk, length, split_on_silence, noise_threshold+4)
                chunks = chunks[:i-1] + subchunks + chunks[i+1:]

    return chunks


def __preprocess_audio(audio):
    return audio.set_sample_width(2).set_channels(1).set_frame_rate(8000)


def transcribe(path, api_key):
    logging.info("Transcribing file %s", path)
    audio = AudioSegment.from_file(path)

    chunks = __generate_chunks(__preprocess_audio(audio))
    logging.debug("Got %d chunks", len(chunks))

    for i, chunk in enumerate(chunks):
        logging.debug("Transcribing chunk %d", i)
        r = __transcribe_chunk(chunk, api_key)

        logging.debug(r)

        if r is not None:
            yield r
